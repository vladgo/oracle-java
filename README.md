#### Simple Oracle Java installer ansible role

[![build status](https://gitlab.com/vladgo/oracle-java/badges/master/build.svg)](https://gitlab.com/vladgo/oracle-java/commits/master)

##### Role variables

* oracle_java_version: 8
* oracle_java_version_update: "102"
* oracle_java_version_build: "14"
* oracle_java_home: "/usr/java/jdk1.{{ oracle_java_version }}.0_{{ oracle_java_version_update }}"
* oracle_java_rpm_filename: "jdk-{{ oracle_java_version }}u{{ oracle_java_version_update }}-linux-x64.rpm"
* oracle_java_rpm_url: "http://download.oracle.com/otn-pub/java/jdk/{{ oracle_java_version }}u{{ oracle_java_version_update }}-b{{ oracle_java_version_build }}/{{ oracle_java_rpm_filename }}"
