# Dockerfile for building centos image with oracle java.
#
# Version  1.0
#

# pull base image
FROM fedora

MAINTAINER Vladislav Gorbunov <vadikgo@gmail.com>

ENV TINI_VERSION 0.13.1

# Use tini as subreaper in Docker container to adopt zombie processes
RUN curl -fsSL https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini-static -o /bin/tini && chmod +x /bin/tini

COPY . /tmp/oracle-java
COPY ./testrun /testrun
RUN set -x \
    && yum install ansible -y \
    && cd /tmp/oracle-java \
    && ansible-playbook test-oracle-java.yml -e 'ansible_python_interpreter=/usr/bin/python3' \
    && cd / \
    && rm -rf /tmp/oracle-java

ENTRYPOINT ["/bin/tini", "--", "/testrun"]
